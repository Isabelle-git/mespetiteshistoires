<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class TalesController extends AbstractController
{
    public function index()
    {
        return $this->render('tales/index.html.twig') ;
    }

    public function add()
    {
        return $this->render('tales/add.html.twig');
    }

    public function show($url)
    {
        return $this->render('tales/show.html.twig', [
            'slug' => $url
        ]);
    }

    public function edit($id)
    {
        return $this->render('tales/edit.html.twig');
    }

    public function remove($id)
    {
        return new Response('<h1> Supprimer l\'article ' .$id. ' : </h1>');
    }


}
