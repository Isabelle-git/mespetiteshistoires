Creation projet Contes pour enfants

$ cd c:/wamp64/www
$ symfony new MesPetitesHistoires --full
$ cd MesPetitesHistoires
$ composer req symfony/webpack-encore-bundle     -> Génère le répertoire asset
$ composer req --dev symfony/profiler-pack      -> Ajoute la debug-bar
$ symfony serve -d          -> Ouvre le serveur en arrière-plan
""

Ouvrir le projet dans l'IDE 
Dans notre application nous avons le fichier 
"bin" : Va contenir des executables qui va nous permettre d'effectuer certaines opérations.
* console pour donner des indications dans le terminal.
* phpUnit pour lancer les tests unitaires

"config": configuration du projet. à l'intérieur, des fichiers au format .yaml 
* packages : différents packages: connexion à la DB, le routing, le système de mailer ...
* routes : 


"public": racine du serveur web
"src": IMPORTANT va contenir le code de l'application
"templates" : IMPORTANT va contenir les vues de l'application. fichiers .twig
"tests" : tests unitaires et fonctionnels
"translations": traductions pour le site
"var" : fichiers cache et les log
"vendor" : dossier propre à composer



Modifier le fichier .env pour changer le chemin vers la BDD

Configurer les routes : 
dossier #Config > routes.yaml

Ajouter les routes :

homepage:
  path: /
  controller: App\Controller\TalesController::index

article_add:
  path: /add
  controller: App\Controller\TalesController::add

article_show:
  path: /show/{url}
  controller: App\Controller\TalesController::show

article_edit:
  path: /edit/{id}
  controller: App\Controller\TalesController::edit
  requirements:
    id: '\d+'

article_remove:
  path: /remove/{id}
  controller: App\Controller\TalesController::remove
  requirements:
    id: '\d+'
    
    
    
*Indications :
Sur les 2 premières routes on a l'accueil et l'ajout, ensuite nous avons la route pour afficher l'article article_show
qui reçoit un paramètre qui est celui de L’URL de l'article, puis nous avons les routes article_edit et article_remove
qui elles, prennent plutôt l'id de l'article, et dans la section requirements on spécifie bien que l'id ne peut être
qu'un entier id: '\d+'.




